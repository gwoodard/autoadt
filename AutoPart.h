/*

George Woodard
CS 215 Fall 2014
Programming Assignment 
10/14/2014
 
This is the Auto Part ADT specification

*/

#include <string>
using namespace std;

class AutoPart
{

public:
	AutoPart();//constructor
	AutoPart(int size);//declare's AutoPart length
	~AutoPart();//this line destroys/deletes the AutoPart
	bool AutoPartIsEmpty() const;//this is a read only decleration and checks if the AutoPart is Empty
	int AutoPartLength() const;//this is a read only decleration and checks the AutoPart length
	void AutoPartInsert(int newPosition, string newItem, bool &success);
	void AutoPartDelete(int position, bool &success);
	void AutoPartRetrieve(int position, string &dataItem, bool &success) const;
	
	
	
private:
	int size;
	string items[15];//decleration of an array with 15 cells
	int index(int position) const;

};

struct partList{
	string partNumber;
	float partCost;
	string partName;
};

typedef partList myType;

//End of AutoPart Class