# Auto ADT #

### **About:** ###

Class assignment on Abstract Data Types. This assignment inserts parts into a partList consisting of partName, partCost and partNumber into an array. Each one is then displayed based on the position in the array.

### **IDE:**
Visual Studio 

### **Input:** ###
Declared in the main CPP file.

### **Language:** ###
C++