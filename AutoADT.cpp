/*

George Woodard
CS 215 Fall 2014
Programming Assignment 
10/14/2014
 
This is the AutoPart ADT implementation/definition

*/

#include "AutoPart.h"
#include <string>
using namespace std;

struct partList{
	string partNumber;
	float partCost;
	string partName;
};

AutoPart::AutoPart(){//constructor method
	size = 0;
}

AutoPart::AutoPart(int s){//destuctor method
	size = s;
}

AutoPart::~AutoPart(){ //this line destroys/deletes the AutoPart
	bool success;
	while (!AutoPartIsEmpty())// the while loop with continue to delete the AutoPart/array until it is 0
		AutoPartDelete(1, success); //this line says once the AutoPart is deleted, it will be finished 
}

bool AutoPart::AutoPartIsEmpty() const{ //this line ask if the AutoPart is empty
	return bool(size == 0); //this will check if the AutoPart is empty and if the size value equals to 0 then it will return true
}

int AutoPart::AutoPartLength() const{
	return size;
}

void AutoPart::AutoPartInsert(int newPos, string newItem, bool &success){

	success = bool(newPos >= 1) && (newPos <= size + 1) && (size < 10);
	if (success)
	{
		for (int position = size; position >= newPos; --position)
			items[index(position +1)] = items[index(position)];
		items[index(newPos)] = newItem;
		++size;
	}
}

void AutoPart::AutoPartDelete(int position, bool &success)
{
	success = bool(position >=1) && (position <= size);
	if(success)
	{
		for(int fromPosition = position + 1; fromPosition <= size; ++fromPosition)
			items[index(fromPosition-1)] = items[index(fromPosition)];
		--size;
	}

}

void AutoPart::AutoPartRetrieve(int position, string &dataItem, bool &success) const{
	success = bool((position >= 1) && (position <= size));
	if (success)
	{
		dataItem = items[index(position)];
	}
	
}

int AutoPart::index(int position) const{
	return position -1;

}
