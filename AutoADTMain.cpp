/*

George Woodard
CS 215 Fall 2014
Programming Assignment 
10/14/2014


This is the AutoPart ADT main program

*/

#include <iostream>
#include <string>
#include "AutoPart.h"
using namespace std;

int main()
{
	//start of main program
	//object instantiation
	
	
	AutoPart listAutoPart;
	myType dataItem;
	myType partList[15];

	partList[0].partName = "engine";
	partList[0].partCost = 1200.00;
	partList[0].partNumber = "A1200";

	partList[1].partName = "door";
	partList[1].partCost = 300.00;
	partList[1].partNumber = "A1201";

	partList[2].partName = "window";
	partList[2].partCost = 200.00;
	partList[2].partNumber = "A1202";
		
	partList[3].partName = "spark plug";
	partList[3].partCost = 12.00;
	partList[3].partNumber = "A1203";

	partList[4].partName = "lock";
	partList[4].partCost = 600.00;
	partList[4].partNumber = "A1204";

	partList[5].partName = "seat";
	partList[5].partCost = 300.00;
	partList[5].partNumber = "A1205";
	
	partList[6].partName = "mirror";
	partList[6].partCost = 120.00;
	partList[6].partNumber = "A1206";

	partList[7].partName = "tire set";
	partList[7].partCost = 400.00;
	partList[7].partNumber = "A1207";

	partList[8].partName = "fan";
	partList[8].partCost = 400.00;
	partList[8].partNumber = "A1208";

	partList[9].partName = "brakes";
	partList[9].partCost = 160.00;
	partList[9].partNumber = "A1209";

	partList[10].partName = "radiator";
	partList[10].partCost = 250.00;
	partList[10].partNumber = "A1210";

	partList[11].partName = "fuel pump";
	partList[11].partCost = 300.00;
	partList[11].partNumber = "A1211";

	partList[12].partName = "gas tank";
	partList[12].partCost = 260.00;
	partList[12].partNumber = "A1212";

	partList[13].partName = "gas cap";
	partList[13].partCost = 40.00;
	partList[13].partNumber = "A1213";

	partList[14].partName = "headlight assembly";
	partList[14].partCost = 100.00;
	partList[14].partNumber = "A1214";
		

	bool success;
	int j = 15;
	int k;
	int x = 3;
	
	
	for (k = 0; k < j; k++)
	{
		listAutoPart.AutoPartInsert(k + 1, partList[15], success);
		cout<<"\n Auto Part Items "<<partList[15]<<" is inserted at position "<<k + 1<<": ";
	}
	
	
	for (k = 0; k < listAutoPart.AutoPartLength(); k++)
	{
		listAutoPart.AutoPartRetrieve(k + 1, dataItem, success);
		cout<<"\n Auto Part item retrieved from position "<<k + 1<<": "<<dataItem<<"\n";
	}

	system("pause");



	return 0;
}